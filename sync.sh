#!/usr/bin/env bash

# set -e
# set -x

# repos-to-sync.txt has the following format
#     directory:alias
# where colon is used as separator

sync_dir=$PWD
repos="$(cat repos-to-sync.txt)"

# repos_dict has the following format
# repos_dict[alias]=directory
declare -A repos_dict

for repo in $repos; do
    alias=$(echo $repo | sed s/.*://g) # strip leading including separator
    dir=$(echo $repo | sed s/:.*//g)   # strip trailing including separator
    repos_dict[$alias]=$dir
done

for alias in "${!repos_dict[@]}"; do
    dir=${repos_dict[$alias]}
    echo
    echo Syncing $dir repository/script using $alias
    type=$(file $dir)

    case "$type" in
      *directory* )
	  if [ -d $dir/.git ]; then
	      (cd $dir && git archive --output $sync_dir/git-$alias.tar.xz --prefix=$alias/ HEAD)
	      echo output file git-$alias.tar.xz
	  else
	      tar cfj $sync_dir/dir-$alias.tar.xz --transform "s,^./,$alias/," --directory=$dir . 2>/dev/null
	      echo output file dir-$alias.tar.xz
	  fi
        ;;
      *text* )
	  dirname=$(dirname $dir | sed "s,^/,,1")
	  tar cfj $sync_dir/file-$alias.tar.xz --transform "s,^$dirname/,$alias/," $dir 2>/dev/null
	  echo output file file-$alias.tar.xz
        ;;

      *)
          echo FILE/DIR NOT FOUND $dir
        ;;
    esac
done

